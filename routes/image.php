<?php 

$router->group(function () use ($router) {
    $router->post('/post','PostController@listPost');
    $router->post('/comment', 'CommentController@listComment');
    $router->post('/likes', 'LikeController@postLikes');

});
