<?php 

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller;

class LikeController extends Controller
{
    public function postLikes($id)
    {
        $likes = User::findOrFail($id)->postlikes()->get();
        return ($likes);
    }
}