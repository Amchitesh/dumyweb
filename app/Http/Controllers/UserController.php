<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $sample = $this->validate($request,[
            'name'=>'string|required',
            'username'=>'string|required',
            'email' => 'string|email|unique:users|required',
            'password' => 'string|min:8|required'
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->save();

        if($user->save())
        {
            $sample = response()->json([ 'response', [
                'message' => 'Successfully registered',
                'userId' => $user->id
                ]
            ],201 );
        } 
        return $sample;

    }

    //public function login(Request $request)
    //{
    //    $input = $this->validate($request, [
    //        'email' => ' string|email|unique:users|required',
    //        'password' => 'string|min:8|required'
    //    ]);

        
    //}

}
