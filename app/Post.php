<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $fillable = [
        'id', 'user_id', 'caption','content'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function postlike()
    {
        return $this->hasMany('App\PostLikes');
    }

    public function comment()
    {
        return $this->hasMany('App\Post');
    }
}
